package main

import (
	"fmt"
	"time"

	"github.com/armadanasar/learngopg/db"
	"github.com/go-pg/pg"
)

func main() {
	pgDB := db.Connect()
	// SaveProduct(pgDB)
	db.PlaceholderDemo(pgDB)
	db.PlaceholderDemoNamed(pgDB)
	// DeleteItem(pgDB)
	UpdateItem(pgDB)
	// GetByID(pgDB)
}

func SaveProduct(pgDB *pg.DB) error {
	newPI := db.Product{
		Name:  "Produk 5",
		Desc:  "Product 5 desc",
		Image: "This is image path",
		Price: 4.5,
		Features: struct {
			Name string
			Desc string
			Imp  int
		}{
			Name: "F1",
			Desc: "F1 Desc",
			Imp:  3,
		},
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		IsActive:  true,
	}

	newPI2 := db.Product{
		Name:  "Produk 6",
		Desc:  "Product 6 desc",
		Image: "This is image path",
		Price: 4.5,
		Features: struct {
			Name string
			Desc string
			Imp  int
		}{
			Name: "F1",
			Desc: "F1 Desc",
			Imp:  3,
		},
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		IsActive:  true,
	}

	items := []*db.Product{&newPI, &newPI2}
	err := newPI.SaveMultiple(pgDB, items)
	if err != nil {
		fmt.Printf("error bulk insert product %+v\n\n")
		return err
	}

	return nil
}

func DeleteItem(pgDB *pg.DB) error {
	newPI := &db.Product{Name: "Produk 6"}
	return newPI.DeleteItem(pgDB)
}

func UpdateItem(pgDB *pg.DB) error {
	newPI := &db.Product{ID: 1, Price: 66, IsActive: false}
	return newPI.UpdatePrice(pgDB)
}

func GetByID(pgDB *pg.DB) error {
	newPI := &db.Product{ID: 1, Name: "Produk A"}
	err := newPI.GetByID(pgDB)
	if err != nil {
		fmt.Println("cant get select")
	}

	fmt.Printf("value selected = %v\n\n", newPI)

	return err
}
