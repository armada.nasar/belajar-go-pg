package db

import (
	"fmt"
	"time"

	pg "github.com/go-pg/pg"
)

func Connect() *pg.DB {
	options := &pg.Options{
		User:         "armadanasar",
		Password:     "example",
		Addr:         "localhost:5432",
		Database:     "learngopg",
		DialTimeout:  30 * time.Second, // connection init timeout
		ReadTimeout:  1 * time.Minute,  // for select timeout
		WriteTimeout: 1 * time.Minute,  // for update and insert timeout
		IdleTimeout:  30 * time.Minute, // culling db connection pool
		MaxConnAge:   1 * time.Minute,  // ping timeout
		PoolSize:     20,               // max connection count
	}

	db := pg.Connect(options)
	if db == nil {
		panic("cant connect to db")
	}

	fmt.Println("connected to db")
	CreateProdItemsTable(db)

	return db
}
