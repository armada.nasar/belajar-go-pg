package db

import (
	"fmt"

	pg "github.com/go-pg/pg"
)

type Params struct {
	Param1 string
	Param2 string
}

func PlaceholderDemoNamed(db *pg.DB) error {
	var value string

	params := Params{
		Param1: "wkwk 1",
		Param2: "param 2",
	}

	var query string = "SELECT ?param1"

	// model is the data we are expecting to query
	// go-pg finds and replace ? with value next to param
	_, err := db.Query(pg.Scan(&value), query, params)
	if err != nil {
		fmt.Printf("error querying, %+v\n", err)
	}

	fmt.Printf("value named = %s\n", value)
	return nil
}

func PlaceholderDemo(db *pg.DB) error {
	var value int
	//singular select
	// var query string = "SELECT ?"

	//multyiple select by idx
	var query string = "SELECT ?1"

	// model is the data we are expecting to query
	// go-pg finds and replace ? with value next to param
	_, err := db.Query(pg.Scan(&value), query, 42, 41)
	if err != nil {
		fmt.Printf("error querying, %+v\n", err)
	}

	fmt.Printf("value = %d\n", value)
	return nil
}
