package db

import (
	"fmt"
	"log"
	"time"

	pg "github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

type Product struct {
	RefPtr   int     `sql:"-"`
	ID       int     `sql:"id,pk"`
	Name     string  `sql:"name,unique"`
	Desc     string  `sql:"desc"`
	Image    string  `sql:"image"`
	Price    float64 `sql:"price,type:real"`
	Features struct {
		Name string
		Desc string
		Imp  int
	} `sql:"features,typs:jsonb"`
	CreatedAt time.Time `sql:"created_at"`
	UpdatedAt time.Time `sql:"updated_at"`
	IsActive  bool      `sql:"is_active"`

	tableName struct{} `sql:"product_items_collection"`
}

func (pi *Product) GetByID(db *pg.DB) error {
	var items []*Product

	selectErr := db.
		Model(&items).
		Column("name", "desc"). // select few columns only
		Where("id = ?", pi.ID). // can also be Where("id = ?0", pi.ID).
		Where("name = ?name").  // appending Where turns it into where and
		WhereOr("id = ?", 3).   // appending WhereOr makes it where or
		Limit(1).
		Offset(1).        //pagination with limit and offset
		Order("id desc"). // order by column
		Select()

	if selectErr != nil {
		log.Printf("error while selecting %+v\n\n", selectErr)
		return selectErr
	}

	log.Printf("success select product %v", items)

	return selectErr
}

func (pi *Product) UpdatePrice(db *pg.DB) error {
	tx, txErr := db.Begin()
	defer tx.Rollback()

	if txErr != nil {
		fmt.Printf("cant start trx %+v\n\n", txErr)
		return txErr
	}

	result, updateErr := tx.
		Model(pi).
		Set("price = ?price").
		Where("id = ?id").
		Update()

	if updateErr != nil {
		log.Printf("error while updating %+v\n\n", updateErr)
		tx.Rollback()
		return updateErr
	}

	result, updateErr = tx.
		Model(pi).
		Set("is_active = ?", false).
		Where("id = ?id").
		Update()

	if updateErr != nil {
		log.Printf("error while updating %+v\n\n", updateErr)
		tx.Rollback()
		return updateErr
	}

	commitErr := tx.Commit()
	if commitErr != nil {
		fmt.Printf("cant commit trx %+v\n\n", commitErr)
		return commitErr
	}

	log.Printf("success update product, result %+v", result)

	return updateErr
}

func (pi *Product) DeleteItem(db *pg.DB) error {
	result, deleteErr := db.Model(pi).Where("name = ?name").Delete()

	if deleteErr != nil {
		log.Printf("error while deleting %+v\n\n", deleteErr)
		return deleteErr
	}

	log.Printf("success delete product, result %+v", result)

	return deleteErr
}

func (pi *Product) SaveAndReturn(db *pg.DB) (*Product, error) {
	result, insertErr := db.Model(pi).Returning("*").Insert()

	if insertErr != nil {
		log.Printf("error while inserting %+v\n\n", insertErr)
		return nil, insertErr
	}

	log.Printf("success insert product, result %+v", result)

	return pi, insertErr
}

func (pi *Product) SaveMultiple(db *pg.DB, items []*Product) error {
	_, insertErr := db.Model(items[0], items[1]).Insert()

	if insertErr != nil {
		log.Printf("error while bulk inserting %+v\n\n", insertErr)
		return insertErr
	}

	log.Printf("success bulk insert product")

	return insertErr
}

func (pi *Product) Save(db *pg.DB) error {
	insertErr := db.Insert(pi)

	if insertErr != nil {
		log.Printf("error while inserting %+v\n\n", insertErr)
		return insertErr
	}

	log.Println("success insert product")

	return insertErr
}

func CreateProdItemsTable(db *pg.DB) error {
	opts := &orm.CreateTableOptions{
		IfNotExists: true,
	}

	createErr := db.CreateTable(&Product{}, opts)
	if createErr != nil {
		fmt.Println("error creating table")
	}

	fmt.Println("success creating table")

	return createErr
}
